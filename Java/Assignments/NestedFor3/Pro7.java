

class Pattern {

	public static void main(String[] args) {
		
		int row = 6;

		for(int i=1; i<=row; i++) {
			
			char ch = 65;
			ch+=row;
			ch-=i;
			for(int j=1; j<=i; j++) {
			
				System.out.print(ch++ +" ");
			}
			System.out.println();
		}
	}
}
