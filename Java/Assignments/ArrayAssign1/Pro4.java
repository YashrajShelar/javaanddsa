
import java.io.*;

class ArrayDemo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());
		
		char arr[] = new char[size];
		System.out.println("Enter array Element");
		
		for(int i=0; i<arr.length; i++) {
		
			arr[i] = (char)br.read(); 
			br.skip(1);
		}
		System.out.print("Vowels are: ");
		for(int i=0; i<arr.length; i++) {

			if(arr[i]=='a' || arr[i]=='A') {
			
				System.out.print(arr[i]+" ");
			} else if(arr[i]=='e' || arr[i]=='E') {
			
				System.out.print(arr[i]+" ");
			} else if(arr[i]=='i' || arr[i]=='I') {
			
				System.out.print(arr[i]+" ");
			} else if(arr[i]=='o' || arr[i]=='O') {
			
				System.out.print(arr[i]+" ");
			} else if(arr[i]=='u' || arr[i]=='U') {
			
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}
