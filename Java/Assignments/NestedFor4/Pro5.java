

class Pattern {

	public static void main(String[] args) {
		
		int row = 4;

		for(int i=1; i<=row; i++) {
		
			char ch = 64;
			ch+=i;
			for(int j=1; j<=row-i+1; j++) {
			
				System.out.print(ch+" ");
				ch++;
			}
			System.out.println();
		}
	}
}
