

class Pattern {

	public static void main(String[] args) {
		
		int row = 5;
		int num = row*(row+1)/2;
		char ch = 64;
		ch+=num;
		for(int i=1; i<=row; i++) {
		
			for(int j=1; j<=i; j++) {
			
				System.out.print(ch-- +" ");
			}
			System.out.println();
		}
	}
}
